

openapi_generator_version=v6.2.1
remote_openapi_file=https://headless.dataddo.com/docs/swagger.yaml

go_gen_garbage=test/ git_push.sh .travis.yml


go-sdk:
	docker run --rm \
		--user ${UID}:${GID} \
		-v "${ROOT_DIR}:/local" \
		openapitools/openapi-generator-cli:${openapi_generator_version} generate \
        	-i ${remote_openapi_file} \
        	-g go \
        	--git-host "${GIT_HOST}" --git-user-id "${GIT_USER_ID}" --git-repo-id "${GIT_REPO_ID}" \
        	-p packageName=capi \
        	-p isGoSubmodule=true \
        	-o /local/capi
	cd ${ROOT_DIR}/capi && rm -r ${go_gen_garbage} && go mod tidy
	cd ${ROOT_DIR} && go mod tidy
